import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:moffee_flutter/Constants.dart';
import 'package:moffee_flutter/config/AppState.dart';
import 'package:moffee_flutter/models/MyHttpResponse.dart';
import 'package:moffee_flutter/models/User.dart';

import 'widgets/screen/MapScreen.dart';
import 'widgets/screen/SpotScreen.dart';
import 'widgets/screen/RegisterSpotScreen.dart';
import 'package:http/http.dart' as http;

void main() async => {
  await loadUser(),
  runApp(MyApp()),
};

Future loadUser() async {
  AppState appState = AppState.getInstance();
  final response = await http.get('${Url.USER_GET}?key=${appState.userKey}');
  print(response.body);
  if (response.statusCode == 200) {
    MyHttpResponse myResponse = MyHttpResponse.fromJson(json.decode(response.body));
    User user = User.fromJson(myResponse.responseData);
    print('user: ' + user.toString());
    appState.user = user;
    print(user.firstName);
  } else {
    throw Exception('Failed to load User');
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: SpotScreen.PATH,
      routes: {
        MapScreen.PATH: (context) => MapScreen(),
        SpotScreen.PATH: (context) => SpotScreen(),
        RegisterSpotScreen.PATH: (context) => RegisterSpotScreen(),
      },
    );
  }
}
