class Constants {
  static const String APP_NAME = 'Moffee';
}

class Url {

  static const String BASE = 'http://192.168.0.103:8080';

  // user
  static const String USER_GET = BASE + '/user/get';

  // spot
  static const String SPOT_REGISTER = BASE + '/spot/submitRegistration';

}