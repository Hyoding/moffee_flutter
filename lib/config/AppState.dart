import 'package:moffee_flutter/models/User.dart';

class AppState {
  static AppState _instance;
  AppState._();
  static AppState getInstance() {
    if (_instance == null) {
      _instance = AppState._();
    }
    return _instance;
  }
  
  // TODO: should be retried from local storage after registering
  String userKey = '07c78d74-24f9-47a5-8240-f51af959d7a1';
  User user;

}