import 'dart:io';
import 'package:image_picker/image_picker.dart';

class MyImagePicker {
  final picker = ImagePicker();
  Future<File> getImage() async {
    final PickedFile pickedFile = await picker.getImage(source: ImageSource.camera);
    return File(pickedFile.path);
  }
}
