import 'package:flutter/material.dart';

class UiUtil {

  static void showSnackBar(BuildContext context, String text) {
    final snackBar = SnackBar(content: Text(text,
      textAlign: TextAlign.center,
    ));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  static void showAlert({@required BuildContext context, String title, String text, Function callBack}) {
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(text),
      actions: [
        FlatButton(
          child: Text('ok'),
          onPressed: () => {
            Navigator.of(context).pop(),
            if (callBack != null) {
              callBack.call(),
            },
          },
        ),
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}