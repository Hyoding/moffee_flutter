import 'package:flutter/material.dart';
import 'package:moffee_flutter/config/AppState.dart';
import 'package:moffee_flutter/models/User.dart';
import 'package:moffee_flutter/widgets/UIUtil.dart';
import '../ImagePicker.dart';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:moffee_flutter/Constants.dart';

class RegisterSpotScreen extends StatefulWidget {
  static const APP_BAR_TITLE = 'Spot Registration';
  static const String PATH = '/screen/register';
  @override
  _RegisterSpotScreenState createState() => _RegisterSpotScreenState();
}

class _RegisterSpotScreenState extends State<RegisterSpotScreen> {
  MyImagePicker imagePicker = MyImagePicker();

  // Form
  final _formKey = GlobalKey<FormState>();

  // Fields
  final TextEditingController _tcStreestAddress = TextEditingController();
  final TextEditingController _tcUnit = TextEditingController();
  final TextEditingController _tcCity = TextEditingController();
  final TextEditingController _tcPostalCode = TextEditingController();
  final TextEditingController _tcCountry = TextEditingController();
  final TextEditingController _tcNote = TextEditingController();

  // Files
  File _imageId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(RegisterSpotScreen.APP_BAR_TITLE)
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  'Address',
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.subtitle1
                ),
              ),
              TextFormField(
                decoration: InputDecoration(hintText: 'Street Address'),
                controller: _tcStreestAddress,
                validator: validateField,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: 'Unit'),
                controller: _tcUnit,
                validator: validateField,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: 'City'),
                controller: _tcCity,
                validator: validateField,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: 'Postal Code'),
                controller: _tcPostalCode,
                validator: validateField,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: 'Country'),
                controller: _tcCountry,
                validator: validateField,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: 'Note'),
                controller: _tcNote,
              ),
              Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: Column(
                  children: [
                    Text(
                      'Government issued ID with address',
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.subtitle1
                    ),
                    Container(
                      width: 400,
                      height: 300,
                      child: _imageId == null
                        ? Icon(Icons.add_a_photo)
                        : Image.file(_imageId),
                    ),
                    RaisedButton(
                      child: Text('Take picture'),
                      onPressed: takePicture,
                    ),
                  ],
                ),
              ),
              
              
              Builder(
                builder: (context) => Padding(
                padding: EdgeInsets.only(top: 50.0),
                  child: RaisedButton(
                    child: Text('Submit'),
                    onPressed: () => submitForm(context),
                  ),
                ),
              ),
            ],
          )
        ),
      ),
    );
  }

  String validateField(String value) {
    if (value.isEmpty) {
      return 'Please enter text';
    }
    print('validate: ' + value);
    return null;
  }

  void takePicture() async {
      File file = await imagePicker.getImage();
      setState(() => {
        _imageId = file,
      });
  }

  void submitForm(BuildContext context) async {
    print('Submitting form');
    if (!isFormReady()) {
      UiUtil.showSnackBar(context, 'Please complete all required fields and pictures');
      return;
    }

    AppState appState = AppState.getInstance();
    User user = appState.user;
    var formData = FormData();
    formData.fields.addAll([
      MapEntry("userId", user.id.toString()),
      // address
      MapEntry("streetAddress", _tcStreestAddress.text),
      MapEntry("unit", _tcUnit.text),
      MapEntry("city", _tcCity.text),
      MapEntry("postalCode", _tcPostalCode.text),
      MapEntry("country", _tcCountry.text),
      MapEntry("note", _tcNote.text),
    ]);
    formData.files.addAll([
      MapEntry("files", await MultipartFile.fromFile(_imageId.path)),
    ]);
    Dio dio = Dio();
    var response = await dio.post(Url.SPOT_REGISTER, data: formData);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.data}');
    UiUtil.showAlert(
      context: context,
      title: 'Spot Form Sent!',
      text: 'You will receive an email once spot is approved',
      callBack: () => Navigator.pop(context),
    );
  }

  bool isFormReady() {
    if (!_formKey.currentState.validate() || _imageId == null) {
      return false;
    }
    return true;
  }

}
