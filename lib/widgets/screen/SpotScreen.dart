import 'package:flutter/material.dart';
import 'package:moffee_flutter/config/AppState.dart';
import 'package:moffee_flutter/models/Address.dart';
import 'package:moffee_flutter/models/Spot.dart';

import 'BaseDrawerScreen.dart';
import 'RegisterSpotScreen.dart';

class SpotScreen extends BaseDrawerScreen {
  static const String PATH = '/spot';
  @override
  Widget getBody(BuildContext context) {
    AppState appState = AppState.getInstance();
    List<SpotWidget> spots = appState.user.spots.map((e) => SpotWidget(spot: e)).toList();
    return Scaffold(
      body: Container(
        child: ListView.builder(
          itemBuilder: (_, index) => spots[index],
          itemCount: spots.length,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, RegisterSpotScreen.PATH),
        tooltip: 'Add a new spot',
        child: Icon(Icons.add),
      ),
    );
  }

  @override
  String getTitle() {
    return "Spot";
  }

  // write a method to build a list of spots from server

}

class SpotWidget extends StatelessWidget {
  final Spot spot;
  SpotWidget({this.spot});
  @override
  Widget build(BuildContext context) {
    Address address = spot.address;
    return Container(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
      height: 100,
      width: double.maxFinite,
      child: FlatButton(
        child: Card(
          elevation: 5,
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(address.streetAddress),
                Text('${address.city}, ${address.province}, ${address.country}'),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Text(spot.status)
                ),
              ],
            ),
          ),
        ),
        onPressed: () => print('gg'),
      ),
    );
  }
}