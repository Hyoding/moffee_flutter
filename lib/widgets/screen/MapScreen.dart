import 'package:flutter/material.dart';

import 'BaseDrawerScreen.dart';

class MapScreen extends BaseDrawerScreen {
  static const String PATH = '/map';

  @override
  Widget getBody(BuildContext context) {
    return Text('Map testing');
  }

  @override
  String getTitle() {
    return "Map";
  }

}
