class Parker {
  String firstName;
  String lastName;
  String email;

  Parker({this.firstName, this.lastName, this.email});

  factory Parker.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return Parker(
      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['email'],
    );
  }
}