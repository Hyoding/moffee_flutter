class Address {
  String streetAddress;
  String unit;
  String city;
  String province;
  String country;
  String postalCode;

  Address({this.streetAddress, this.unit, this.city, this.province, this.country, this.postalCode});

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      streetAddress: json['streetAddress'],
      unit: json['unit'],
      city: json['city'],
      province: json['province'],
      country: json['country'],
      postalCode: json['postalCode'],
    );
  }
}