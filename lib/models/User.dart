import 'package:moffee_flutter/models/Car.dart';
import 'package:moffee_flutter/models/Spot.dart';

class User {
  int id;
  String key;
  String firstName;
  String lastName;
  String email;
  String hashedPassword;
  String type;
  List<Car> cars;
  List<Spot> spots;
  int referredBy;
  double balance;
  double totalCharged;
  double amtInBucket;
  double totalEarned;

  User({
    this.id,
    this.key,
    this.firstName,
    this.lastName,
    this.email,
    this.hashedPassword,
    this.type,
    this.cars,
    this.spots,
    this.referredBy,
    this.balance,
    this.totalCharged,
    this.amtInBucket,
    this.totalEarned
  });

  factory User.fromJson(Map<String, dynamic> json) {
    final List carList = json['cars'];
    final List spotList = json['spots'];
    return User(
      id: json['id'],
      key: json['key'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['email'],
      hashedPassword: json['hashedPassword'],
      type: json['type'],
      cars: carList.map((car) => Car.fromJson(car)).toList(),
      spots: spotList.map((spot) => Spot.fromJson(spot)).toList(),
      referredBy: json['referredBy'],
      balance: json['balance'],
      totalCharged: json['totalCharged'],
      amtInBucket: json['amtInBucket'],
      totalEarned: json['totalEarned'],
    );
  }
}