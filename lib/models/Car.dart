class Car {
  String licensePlate;
  String model;
  String colour;

  Car({this.licensePlate, this.model, this.colour});

  factory Car.fromJson(Map<String, dynamic> json) {
    return Car(
      licensePlate: json['licensePlate'],
      model: json['model'],
      colour: json['colour'],
    );
  }
}