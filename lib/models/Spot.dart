import 'package:moffee_flutter/models/Address.dart';
import 'package:moffee_flutter/models/Parker.dart';

class Spot {
  Address address;
  double rate;
  String status;
  DateTime availableUntil;
  Parker parker;
  DateTime occupyUntil;

  Spot({this.address, this.rate, this.status, this.availableUntil, this.parker, this.occupyUntil});

  factory Spot.fromJson(Map<String, dynamic> json) {
    return Spot(
      address: Address.fromJson(json['address']),
      rate: json['rate'],
      status: json['status'],
      availableUntil: json['availableUntil'],
      parker: Parker.fromJson(json['parkerDto']),
      occupyUntil: json['occupyUntil'],
    );
  }
}