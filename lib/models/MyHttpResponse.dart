class MyHttpResponse {
  String message;
  dynamic responseData;

  MyHttpResponse({this.message, this.responseData});

  factory MyHttpResponse.fromJson(Map<String, dynamic> json) {
    return MyHttpResponse(
      message: json['message'],
      responseData: json['responseData'],
    );
  }
}